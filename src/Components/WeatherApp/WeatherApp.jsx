import React, { useEffect, useState } from 'react'
import { Container, Row, Col } from 'react-bootstrap'
import './WeatherApp.css'

import search_icon from '../Assets/search.png'
import clear_icon from '../Assets/clear.png'
import cloud_icon from '../Assets/cloud.png'
import drizzle_icon from '../Assets/drizzle.png'
import rain_icon from '../Assets/rain.png'
import snow_icon from '../Assets/snow.png'
import wind_icon from '../Assets/wind.png'
import humidity_icon from '../Assets/humidity.png'
import search_city from '../Assets/search-city.png'
import not_found from '../Assets/not-found.png'

const WeatherApp = () => {

    const owmapi_key = process.env.REACT_APP_OPENWEATHERMAP_API_KEY;
    const usapi_key = process.env.REACT_APP_UNSPLASH_API_KEY;

    const [ wicon, setWicon ] = useState(search_city);
    const [ humidity, setHumidity ] = useState("---");
    const [ wind, setWind ] = useState("---");
    const [ temperature, setTemperature ] = useState("---");
    const [ time, setTime ] = useState("00:00");
    const [ location, setLocation ] = useState("Search City");
    const [ weatherIcon, setWeatherIcon ] = useState("");
    const [ backgroundImg, setBackgroundImg ] = useState("https://images.unsplash.com/photo-1513002749550-c59d786b8e6c?q=80&w=1887&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D");
    const [ fetchFinish, setFetchFinished ] = useState(true);



    const search = () => {
        
        setFetchFinished(false);

        const element = document.getElementsByClassName("cityInput")

        if(element[0].value===""){
            return 0;
        }
        
        fetch(`https://api.openweathermap.org/data/2.5/weather?q=${element[0].value}&units=Metric&appid=${owmapi_key}`)
        .then(res => res.json())
        .then(data => {

            const timezoneOffsetInSeconds = data.timezone;
            const currentTimestamp = new Date().getTime() / 1000;
            const cityCurrentTimestamp = currentTimestamp + timezoneOffsetInSeconds;
            const cityCurrentTime = new Date(cityCurrentTimestamp * 1000);
            setTime(cityCurrentTime.toLocaleTimeString(undefined, { timeZone: 'UTC' }))

            if(data.cod === 200){
                setHumidity(data.main.humidity + " %");
                setWind(Math.floor(data.wind.speed) + " km/h");
                setTemperature(Math.floor(data.main.temp) + "°c");
                setLocation(data.name);
                setWeatherIcon(data.weather[0].icon);
            } else {
                setHumidity("---");
                setWind("---");
                setTemperature("---");
                setLocation("City not Found!");
                setWicon(not_found);
            }

            return fetch(`https://api.unsplash.com/photos/random?query={${element[0].value}}&client_id=${usapi_key}`);
            
        })
        .then(res => res.json())
        .then(data => {

            setBackgroundImg(data.urls.full);
            
            setFetchFinished(true);
        })

    }

    useEffect(() => {

        const weatherImage = document.querySelector('.weather-image');

        if(weatherIcon==="01d" || weatherIcon==="01n"){
            
            setWicon(clear_icon);

        } else if(weatherIcon==="02d" || weatherIcon==="02n"){
            
            setWicon(cloud_icon);

        } else if(weatherIcon==="03d" || weatherIcon==="03n"){
            
            setWicon(drizzle_icon);

        } else if(weatherIcon==="04d" || weatherIcon==="04n"){
            
            setWicon(drizzle_icon);

        } else if(weatherIcon==="09d" || weatherIcon==="09n"){
            
            setWicon(rain_icon);

        } else if(weatherIcon==="10d" || weatherIcon==="10n"){
            
            setWicon(rain_icon);

        } else if(weatherIcon==="13d" || weatherIcon==="13n"){
            
            setWicon(snow_icon);

        }

        if (fetchFinish){
            setTimeout(() => {
                weatherImage.classList.add('animate');
            }, 200)
        } else {
                weatherImage.classList.remove('animate');
        }

    },[fetchFinish, weatherIcon])

  return (
    <Container className='container' fluid>
        <span className='bckImg' style={{
            content: '""',
            position: 'absolute',
            top: 0,
            right: 0,
            bottom: 0,
            left: 0,
            backgroundImage: `url(${backgroundImg})`,
            backgroundSize: 'cover',
            backgroundPosition: 'center',
            filter: 'blur(3px)',
            zIndex: -1
        }}></span>
        <span className='content'>
        <Row className='top-bar'>
            <input type="text" className='cityInput' placeholder='Search'/>
            <div className="search-icon" onClick={()=>{search()}}>
                <img src={search_icon} alt="" />
            </div>
        </Row>
        <Row className="weather-image">
            <img src={wicon} alt="" />
        </Row>
        <Row className="weather-temp"><h1>{temperature}</h1></Row>
        <Row className="weather-location">{location}  {time}</Row>
        <div className="data-container">
            <div className="element">
                <img src={humidity_icon} alt="" className="icon" />
                <div className="data">
                    <div className="humidity-percent">{humidity}</div>
                    <div className="text">Humidity</div>
                </div>
            </div>
            <div className="element">
                <img src={wind_icon} alt="" className="icon" />
                <div className="data">
                    <div className="wind-rate">{wind}</div>
                    <div className="text">Wind Speed</div>
                </div>
            </div>
        </div>
        </span>
    </Container>
  )
}

export default WeatherApp